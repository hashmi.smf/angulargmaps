import { Component } from '@angular/core';
import { Http, Response } from '@angular/http';
import { GOOGLE_MAPS_DIRECTIVES } from 'angular2-google-maps/core';
@Component({
  selector: 'my-app',
  templateUrl: 'app/app.component.html',
  styleUrls: ['app/app.component.css']
  directives: [GOOGLE_MAPS_DIRECTIVES]
})
export class AppComponent {
  message: string;
  title: string = 'My first angular2-google-maps project';
  lati: [17.4565684, 17.4325235];
  lngi: [78.4420864,78.40701519999993]
  lat: number = 17.4565684;
  lng: number = 78.4420864;
  lat1: number = 17.4325235;
  lng1: number = 78.40701519999993;
  constructor(public http: Http){
    this.http.get('http://localhost:3000/api').subscribe(data => this.message = data.json().some,
							err => console.log(err));
  }
}
