import { bootstrap }    from '@angular/platform-browser-dynamic';
import { HTTP_PROVIDERS } from '@angular/http';
import { AppComponent } from './app.component';
import {GOOGLE_MAPS_PROVIDERS} from 'angular2-google-maps/core'
bootstrap(AppComponent, [ HTTP_PROVIDERS, GOOGLE_MAPS_PROVIDERS ]);

